from rest_framework import serializers
from rest_framework.authentication import SessionAuthentication
import datetime


class TagField(serializers.RelatedField):
    tag_object = None

    def __init__(self, tag_object, *args, **kwargs):
        self.tag_object = tag_object
        return super(TagField, self).__init__(*args, **kwargs)

    def to_representation(self, value):
        name_list = value.name
        return name_list

    def to_internal_value(self, data):
        return self.tag_object.objects.get(name=data)

    def get_queryset(self):
        return self.queryset


class TagSerializer(serializers.Serializer):
    id = serializers.CharField(max_length=200)
    value = serializers.CharField(max_length=200)
    real_id = serializers.IntegerField()


class TuppleSerializer(serializers.Serializer):
    id = serializers.CharField(max_length=200)
    value = serializers.CharField(max_length=200)


class TreeSerializer(serializers.Serializer):
    id = serializers.CharField(max_length=200)
    value = serializers.CharField(max_length=200)
    data = serializers.ListField()
    open = serializers.BooleanField()


class CsrfExemptSessionAuthentication(SessionAuthentication):

    def enforce_csrf(self, request):
        return  # To not perform the csrf check previously happening


class NullableDateSerializer(serializers.BaseSerializer):
    """
    This serializer is used for being able to have empty dates
    in webix datatables.

    On empty values, it returns empty string instead of null
    """
    def get_attribute(self, instance):
        ret = super(NullableDateSerializer, self).get_attribute(instance)
        if ret is None:
            return ''
        else:
            return ret

    def to_representation(self, obj):
        # 2017-09-15
        if obj is not None and obj != '':
            return obj.strftime('%Y-%m-%d')
        else:
            return ""

    def to_internal_value(self, obj):
        if obj == '' or obj is None:
            return None
        elif type(obj) == datetime.datetime:
            return obj.date()
        elif type(obj) == datetime.date:
            return obj
        else:
            return datetime.datetime.strptime(obj.split(' ')[0], '%Y-%m-%d')
