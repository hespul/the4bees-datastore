# -*- coding: utf-8 -*-
# from django.contrib.auth.decorators import login_required
# from django.contrib.auth.forms import PasswordResetForm
# from django.contrib.auth.views import password_reset, password_reset_confirm, password_reset_done
# from django.core.urlresolvers import reverse
# from django.http import HttpResponse
# from django.http import HttpResponseRedirect
# from django.shortcuts import redirect
# from django.shortcuts import render
# from django.shortcuts import get_object_or_404
# 
# from django.views.generic.edit import FormView
# from django.views.generic.list import ListView
# from django.views.generic.detail import DetailView
# from django.views.generic.edit import CreateView, FormMixin, ProcessFormView, UpdateView
# 
# 
# from django.core.mail import send_mail
# from django.template import Context, Template
# from django.template.loader import get_template
# from django.contrib.sites.shortcuts import get_current_site
# from django.http import Http404
# 
# 
# from .models import User
# from .forms import RegistrationForm, AccountForm
# 
# 
# 
# def page_not_found(request):
#     return render(request, 'accounts/404.html')
# 
# 
# 
# 
# @login_required
# def AccountView(request):
#     return render(request, 'accounts/account.html')
# 
# 
# class AccountUpdate(UpdateView):
#     model = User
#     context_object_name = 'User'
#     template_name = 'accounts/account.html'
#     form_class = AccountForm
#     success_url = '/'
#     
#     def get_object(self, qs=None):
#         return self.request.user
# 
# 
# 
# 
# class RegistrationView(CreateView):
#     form_class = RegistrationForm
#     model = User
# 
#     def form_valid(self, form):
#         #import pdb; pdb.set_trace()
#         
#         obj = form.save(commit=False)
#         obj.set_password(User.objects.make_random_password())
#         obj.is_self_creation = True
#         obj.save()
# 
#         # This form only requires the "email" field, so will validate.
#         reset_form = PasswordResetForm(self.request.POST)
#         reset_form.is_valid()  # Must trigger validation
#         # Copied from django/contrib/auth/views.py : password_reset
#         opts = {
#             'use_https': self.request.is_secure(),
#             'email_template_name': 'registration/verification.html',
#             'subject_template_name': 'registration/verification_subject.txt',
#             'request': self.request,
#             # 'html_email_template_name': provide an HTML content template if you desire.
#         }
#         # This form sends the email on save()
#         reset_form.save(**opts)
# 
#         return redirect('accounts:register-done')
# 
#     
# 
# def reset_confirm(request, uidb36=None, token=None):
#     return password_reset_confirm(request,
#         uidb36=uidb36, token=token, post_reset_redirect=reverse('app:login'))
# 
# def reset_done(request):
#     return password_reset_done(request)
# 
# def reset(request):
#     return password_reset(request)
# 
