# -*- coding: utf-8 -*-

from django.core.urlresolvers import reverse
from django.utils.translation import ugettext_lazy as _

from cms.menu_bases import CMSAttachMenu
from menus.base import NavigationNode
from menus.menu_pool import menu_pool

from config.settings import ALLOW_USER_SELF_REGISTRATION


class AccountsMenu(CMSAttachMenu):
    name = _("Menu de gestion des comptes")

    def get_nodes(self, request):
        nodes = []

        nodes.append(
            NavigationNode(
                title=_("Connexion"),
                url=reverse('accounts:login'),
                id=1,
                attr={
                    'visible_for_authenticated': False,
                }
            )
        )
        if ALLOW_USER_SELF_REGISTRATION:
            nodes.append(
                NavigationNode(
                    title=_("Créer un compte"),
                    url=reverse('accounts:register'),
                    id=2,
                    attr={
                        'visible_for_authenticated': False,
                    }
                ),
            )

        nodes.append(
            NavigationNode(
                title=_("Éditer mon compte"),
                url=reverse('accounts:account'),
                id=3,
                attr={
                    'visible_for_anonymous': False,
                }
            )
        )
        nodes.append(
            NavigationNode(
                title=_("Changer mon mot de passe"),
                url=reverse('accounts:password_change'),
                id=4,
                attr={
                    'visible_for_anonymous': False,
                }
            )
        )

        nodes.append(
            NavigationNode(
                title=_("Déconnexion"),
                url=reverse('accounts:logout'),
                id=5,
                attr={
                    'visible_for_anonymous': False,
                }
            ),
        )

        return nodes


menu_pool.register_menu(AccountsMenu)
