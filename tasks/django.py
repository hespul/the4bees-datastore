from invoke import task

from tasks.docker import dc


# DJANGO ################################################################################

def __run(ctx, service, command, pty=False):
    cmd = 'python manage.py {}'.format(command)
    dc(ctx, service=service, command='run --rm django {}'.format(cmd), pty=pty)


def __exec(ctx, service, command, pty=False):
    cmd = 'python /app/django/manage.py {}'.format(command)
    dc(ctx, service=service, command='exec django {}'.format(cmd), pty=pty)
    

    
@task(help={
    'service': 'service name',
    'command': 'optionnal command for manage',
})
def run(ctx, service=None, command='help'):
    """
    Django manage.py
    """
    service = service or ctx.service
    __exec(ctx, service=service, command=command, pty=True)


    
@task(help={
    'service': 'service name',
    'command': 'optionnal command for migration',
})
def migrations(ctx, service=None, command=''):
    """
    Make django migrations
    """
    service = service or ctx.service
    __exec(
        ctx=ctx,
        service=service,
        command='makemigrations {}'.format(command),
        pty=True,
    )

@task(help={
    'service': 'service name',
    'command': 'optionnal command for migration',
})
def migrate(ctx, service=None, command=''):
    """
    Do django migrate
    """
    service = service or ctx.service
    __exec(
        ctx=ctx,
        service=service,
        command='migrate {}'.format(command),
        pty=True,
    )


@task(help={
    'service': 'service name',
    'command': 'optionnal command for shell_plus',
})
def c(ctx, service=None, command=''):
    """
    Django shell_plus
    """
    service = service or ctx.service
    __exec(
        ctx=ctx,
        service=service,
        command='shell_plus {}'.format(command),
        pty=True,
    )


