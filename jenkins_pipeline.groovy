#!/usr/bin/env groovy


pipeline {
    agent any
    stages {

        stage("Prepare") {
            steps {
                bitbucketStatusNotify buildState: "INPROGRESS"
            }
        }
        
        stage ('Check source for loose calls to debuggers') {
            steps {
                sh returnStatus: true, script: '[ $(egrep  --exclude-dir=django-simple_perms -r "[w|p]db.set_trace()"  django/ |grep -v "#.*[w|p]db.set_trace()"|wc -l) -eq 0 ]'
            }
        }

        stage ('Pull base images') {
            steps {
                sh 'docker pull registry.hespul.org:5000/the4beesdatastore_django-base'
                sh 'docker push registry.hespul.org:5000/the4beesdatastore_django:$BRANCH_NAME'
                sh 'docker push registry.hespul.org:5000/the4beesdatastore_postgres:$BRANCH_NAME'
                sh 'docker push registry.hespul.org:5000/the4beesdatastore_nginx:$BRANCH_NAME'
            }
        }
        stage ('Compile assets (CSS + mimify)') {
            steps {
                sh 'mkdir -p django/static/css ; chmod 777 django/static/css'
                sh 'docker-compose -f compose/envs/assets_compiler-jenkins.yml pull'
                sh 'docker-compose -p assets_djcms_$BRANCH_NAME -f compose/envs/assets_compiler-jenkins.yml run --rm assets_compiler config mimify:stylus'
                sh 'docker-compose -p assets_djcms_$BRANCH_NAME -f compose/envs/assets_compiler-jenkins.yml run --rm assets_compiler_newsletters config build:stylus mimify:stylus'
                sh 'docker-compose -p assets_djcms_$BRANCH_NAME -f compose/envs/assets_compiler-jenkins.yml run --rm assets_compiler_fac config mimify:stylus'
            }
        }
        stage ('Remove dangling containers') {
            steps {
                sh 'docker rm -f the4beesdatastorejenkins$BRANCH_NAME_elastic_1 || true'
                sh 'docker rm -f the4beesdatastorejenkins$BRANCH_NAME_postgres_1 || true'
                sh 'docker rm -f the4beesdatastorejenkins$BRANCH_NAME_django_1 || true'
            }    
        }
        stage ('Build master images') {
            steps {
                sh 'docker build -f compose/django/Dockerfile-jenkins -t registry.hespul.org:5000/the4beesdatastore_django:jenkins-$BRANCH_NAME --build-arg APP_VERSION=$BUILD_TIMESTAMP ./'
                sh 'docker build -f compose/postgres/Dockerfile -t registry.hespul.org:5000/the4beesdatastore_postgres:jenkins-$BRANCH_NAME --build-arg APP_VERSION=$BUILD_TIMESTAMP ./compose/postgres/'
                sh 'docker build -f compose/nginx/Dockerfile -t registry.hespul.org:5000/the4beesdatastore_nginx:jenkins-$BRANCH_NAME --build-arg APP_VERSION=$BUILD_TIMESTAMP ./compose/nginx/'
            }    
        }
            stage ('Tag original master image as needed') {
                when { anyOf { branch 'master'; branch 'staging' } }
                steps {
                sh '''
docker tag registry.hespul.org:5000/the4beesdatastore_django:jenkins-$BRANCH_NAME registry.hespul.org:5000/the4beesdatastore_django:$BRANCH_NAME
docker tag registry.hespul.org:5000/the4beesdatastore_django:jenkins-$BRANCH_NAME registry.hespul.org:5000/the4beesdatastore_django:$BRANCH_NAME-$GIT_COMMIT
docker tag registry.hespul.org:5000/the4beesdatastore_django:jenkins-$BRANCH_NAME registry.hespul.org:5000/the4beesdatastore_django:$BRANCH_NAME-$BUILD_TIMESTAMP
'''

                sh '''
docker tag registry.hespul.org:5000/the4beesdatastore_postgres:jenkins-$BRANCH_NAME registry.hespul.org:5000/the4beesdatastore_postgres:$BRANCH_NAME
docker tag registry.hespul.org:5000/the4beesdatastore_postgres:jenkins-$BRANCH_NAME registry.hespul.org:5000/the4beesdatastore_postgres:$BRANCH_NAME-$GIT_COMMIT
docker tag registry.hespul.org:5000/the4beesdatastore_postgres:jenkins-$BRANCH_NAME registry.hespul.org:5000/the4beesdatastore_postgres:$BRANCH_NAME-$BUILD_TIMESTAMP
'''

                sh '''
docker tag registry.hespul.org:5000/the4beesdatastore_nginx:jenkins-$BRANCH_NAME registry.hespul.org:5000/the4beesdatastore_nginx:$BRANCH_NAME
docker tag registry.hespul.org:5000/the4beesdatastore_nginx:jenkins-$BRANCH_NAME registry.hespul.org:5000/the4beesdatastore_nginx:$BRANCH_NAME-$GIT_COMMIT
docker tag registry.hespul.org:5000/the4beesdatastore_nginx:jenkins-$BRANCH_NAME registry.hespul.org:5000/the4beesdatastore_nginx:$BRANCH_NAME-$BUILD_TIMESTAMP
'''
                    }    
            }
            stage ('Push images to registry ') {
                when { anyOf { branch 'master'; branch 'staging' } }
                steps {
                sh '''
docker push registry.hespul.org:5000/the4beesdatastore_django:$BRANCH_NAME
docker push registry.hespul.org:5000/the4beesdatastore_django:$BRANCH_NAME-$GIT_COMMIT
docker push registry.hespul.org:5000/the4beesdatastore_django:$BRANCH_NAME-$BUILD_TIMESTAMP
'''

                sh '''
docker push registry.hespul.org:5000/the4beesdatastore_postgres:$BRANCH_NAME
docker push registry.hespul.org:5000/the4beesdatastore_postgres:$BRANCH_NAME-$GIT_COMMIT
docker push registry.hespul.org:5000/the4beesdatastore_postgres:$BRANCH_NAME-$BUILD_TIMESTAMP
'''

                sh '''
docker push registry.hespul.org:5000/the4beesdatastore_nginx:$BRANCH_NAME
docker push registry.hespul.org:5000/the4beesdatastore_nginx:$BRANCH_NAME-$GIT_COMMIT
docker push registry.hespul.org:5000/the4beesdatastore_nginx:$BRANCH_NAME-$BUILD_TIMESTAMP
'''
                    }    
            }
            stage ('Retag latest') {
                when { anyOf { branch 'master' } }
                steps {
                sh '''
docker tag registry.hespul.org:5000/the4beesdatastore_django:$BRANCH_NAME registry.hespul.org:5000/the4beesdatastore_django:latest
docker tag registry.hespul.org:5000/the4beesdatastore_postgres:$BRANCH_NAME registry.hespul.org:5000/the4beesdatastore_postgres:latest
docker tag registry.hespul.org:5000/the4beesdatastore_nginx:$BRANCH_NAME registry.hespul.org:5000/the4beesdatastore_nginx:latest
docker push registry.hespul.org:5000/the4beesdatastore_django:latest
docker push registry.hespul.org:5000/the4beesdatastore_postgres:latest
docker push registry.hespul.org:5000/the4beesdatastore_nginx:latest
'''
                    }    
            }
    }
    post {
      success {
          bitbucketStatusNotify buildState: "SUCCESSFUL"
          notifyBuild("SUCCESSFUL")
      }
 
      failure {
          bitbucketStatusNotify buildState: "FAILED"
          notifyBuild("FAILED")
      }
    }
   }

def notifyBuild(String buildStatus = 'STARTED') {
  // build status of null means successful
  buildStatus =  buildStatus ?: 'SUCCESSFUL' 
  def subject = "${buildStatus}: Job '${env.JOB_NAME} [${env.BUILD_NUMBER}]'"
  def summary = "${subject} (${env.BUILD_URL})"
  def details = """${buildStatus}: Job '${env.JOB_NAME} [${env.BUILD_NUMBER}] on ${env.BRANCH_NAME}':

Check console output at ${env.BUILD_URL}
"""
 
  emailext (
      subject: subject,
      body: details,
      to: '$DEFAULT_RECIPIENTS'
      // recipientProviders: [[$class: 'DevelopersRecipientProvider']]
    )
}
