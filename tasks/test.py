from invoke import task

from tasks.docker import dc


# TESTS #######################################################################

@task(help={
    'service': 'service name [djcms]',
    'command': 'optionnal command arguments',
})
def test(ctx, service=None, command=None):
    """
    Launch test for django
    """
    if not command:
        command = ''

    service = service or ctx.service
    dc(
        ctx,
        service,
        'exec django /bin/bash -c "(cd /app/django ; pytest {0})"'.format(
            command if command else ''
        )
    )
