from django.db.models.deletion import ProtectedError
from django.utils.translation import ugettext_lazy as _
from django.conf import settings
from rest_framework import status
from rest_framework.response import Response
from rest_framework.views import exception_handler
from rest_framework.compat import set_rollback


def custom_exception_handler(exc, context):

    import wdb; wdb.set_trace()
    # Call REST framework's default exception handler first to get the standard error response.
    response = exception_handler(exc, context)
    if response:
        return response

    # Handle ProtectedError exception
    if isinstance(exc, ProtectedError):
        protected_models = {po.__class__._meta.verbose_name.capitalize() for po in exc.protected_objects}
        msg = _("Related objects prevent deletion. "
                "Please delete related objects before : %s") % ', '.join(protected_models)
        data = {'detail': msg}
        set_rollback()
        return Response(data, status=status.HTTP_409_CONFLICT)

    # We handle specific case ValidationError on model's save method. Return dict with concerned fields
    if isinstance(exc, DjangoValidationError):
        data = {'detail': exc.message_dict}
        return Response(data, status=status.HTTP_400_BAD_REQUEST)

    # Handle any other exception ?
    # if isinstance(exc, Exception):
    #     data = {'detail': _("Critical server error")}
    #     set_rollback()
    #     return Response(data, status=status.HTTP_500_INTERNAL_SERVER_ERROR)

    return None

