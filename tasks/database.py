from invoke import task

from tasks.docker import dc



# DATABASE ################################################################################
    
@task(help={
    'service': 'service name [djcms]',
})
def list(ctx, service=None):
    """
    List available backups for service
    """
    service = service or ctx.service
    dc(ctx, service, 'run --rm postgres list-backups')


@task(help={
    'service': 'service name [djcms]',
})
def shell(ctx, service=None):
    """
    Get a psql shell for service
    """
    service = service or ctx.service
    dc(ctx, service, "exec postgres bash -c 'psql -U $POSTGRES_USER'", pty=True)


@task(help={
    'service': 'service name [djcms]',
    'name': 'file to restore',
})
def restore(ctx, name, service=None):
    """
    Restore database for [service] from file [name]
    """
    service = service or ctx.service
    # check running postgres    
    dc(ctx, service, "ps |grep -e 'postgres.*Up'".format(name))

    from tasks.docker import down, upd

    down(ctx, service, 'django')
    dc(ctx, service, 'run --rm postgres restore {}'.format(name))
    upd(ctx, service, 'django')


@task(help={
    'service': 'service name [djcms]',
    'name': 'filename to save data to',
})
def backup(ctx, name=None, service=None):
    """
    Save database for [service] in file [name]
    """
    service = service or ctx.service
    name = name or ctx.service

    # check running postgres    
    dc(ctx, service, "ps |grep -e 'postgres.*Up'".format(name))
    
    dc(ctx, service, 'run --rm postgres backup {}'.format(name))

