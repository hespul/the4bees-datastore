# -*- coding: utf-8 -*-

# ## Installed apps ##################################

INSTALLED_APPS = [
    "accounts",
    "django.contrib.auth",
    "django.contrib.contenttypes",
    "django.contrib.sessions",
    "django.contrib.admin",
    "django.contrib.sites",
    "django.contrib.staticfiles",
    "django.contrib.messages",
    "django_extensions",
    "mail_templated",
    "tagulous",
    "simple_perms",
    "rest_framework",
    "rest_framework.authtoken",
    "corsheaders",
    "background_task",
    "core",
    "cms",
    "sekizai",
    "menus",
    "colorful",
    "treebeard",
    "djangocms_text_ckeditor",
    "datastore",
]

# ## /Installed apps ##################################
