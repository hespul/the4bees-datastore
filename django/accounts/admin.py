# -*- coding: utf-8 -*-
from django.contrib import admin
from django.utils.translation import ugettext_lazy as _

from .models import User, Group


class UserAdmin(admin.ModelAdmin):
    list_display = [
        'email',
        'civility',
        'first_name',
        'last_name',
        'is_staff',
        'is_active',
        'date_joined',
    ]


admin.site.register(User, UserAdmin)


class GroupAdmin(admin.ModelAdmin):
    list_display = [
        'name',
        'domainname',
        'users',
    ]
    def users(self, obj):
        return ', '.join([str(x) for x in obj.get_users()])
    users.short_description = _('Utilisateurs')


admin.site.register(Group, GroupAdmin)
