# -*- coding: utf-8 -*-
from cms.app_base import CMSApp
from cms.apphook_pool import apphook_pool
from django.utils.translation import ugettext_lazy as _

from .cms_menus import AccountsMenu

# import logging
# logger = logging.getLogger(__name__)


class AccountApphook(CMSApp):
    name = _("Comptes")
    app_name = 'accounts'

    def get_urls(self, page=None, language=None, **kwargs):
        return ["accounts.urls"]

    def get_menus(self, page=None, language=None, **kwargs):
        return [AccountsMenu]


apphook_pool.register(AccountApphook)
