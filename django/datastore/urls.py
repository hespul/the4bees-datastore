# -*- coding: utf-8 -*-

from django.conf.urls import url, include
from rest_framework import routers
from .views import (
    DataPointViewSet,
    DataPointAsCSV,
    DataPointDailyAverageViewSet,
    InputDataPointViewSet,
    MessageListViewSet,
)

router = routers.DefaultRouter()
router.register(r'datapoint', DataPointViewSet)
router.register(r'datapoint-avg-day', DataPointDailyAverageViewSet, base_name='datapoint-avg-day')
router.register(r'input-datapoint', InputDataPointViewSet, base_name='input-datapoint')
router.register(r'message', MessageListViewSet, base_name='message')

# Wire up our API using automatic URL routing.
# Additionally, we include login URLs for the browsable API.
urlpatterns = [
    url(r'^', include(router.urls)),
    url(r'^datapoint-as-csv', DataPointAsCSV.as_view()),
    # url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework'))
]

app_name = "datastore"
