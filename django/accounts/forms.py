# -*- coding: utf-8 -*-
from django import forms
from django.db import models
from django.forms import ModelForm
from django.forms import ValidationError
from accounts.models import User

from crispy_forms.helper import FormHelper
from crispy_forms.layout import Submit
from crispy_forms.layout import Layout, Fieldset, ButtonHolder, Submit, Field
from crispy_forms.bootstrap import StrictButton

import datetime
import uuid

from .models import User

from django.utils.translation import ugettext_lazy as _


class AccountForm(forms.ModelForm):
    error_css_class = 'error'
    required_css_class = 'required'
    class Meta:
        model = User
        fields = ('civility', 'first_name', 'last_name')
        exclude = ('email', 'password')
        #next = '/'

    def __init__(self, *args, **kwargs):
        super(AccountForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.add_input(Submit('submit', _('Submit'), css_class=''))
        return

    
class RegistrationForm(forms.ModelForm):
    error_css_class = 'error'
    required_css_class = 'required'

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for field in iter(self.fields):
            self.fields[field].widget.attrs.update({
                'class': 'mdl-textfield__input'
        })

    class Meta:
        model = User
        fields = ['email', 'civility', 'first_name', 'last_name']

    def clean(self):
        """
        Verifies that the values entered into the password fields match

        NOTE: Errors here will appear in ``non_field_errors()`` because it applies to more than one field.
        """
        #import pdb; pdb.set_trace()
        super(RegistrationForm, self).clean()
        if 'password1' in self.cleaned_data and 'password2' in self.cleaned_data:
            if self.cleaned_data['password1'] != self.cleaned_data['password2']:
                raise forms.ValidationError(_("The passwords did not match. Please try again."))
        return self.cleaned_data

    def save(self, commit=True):
        user = super(RegistrationForm, self).save(commit=False)
        if 'password1' in self.cleaned_data:
            user.set_password(self.cleaned_data['password1'])
        else:
            user.set_password(uuid.uuid4())
            
        if commit:
            user.save()
        return user

