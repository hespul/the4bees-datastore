# -*- coding: utf-8 -*-
from django.db import models
from django.utils import timezone
from django.contrib.auth.models import AbstractBaseUser, PermissionsMixin
from django.contrib.auth.models import BaseUserManager

from django.utils.translation import ugettext_lazy as _

import accounts.static_data


class UserManager(BaseUserManager):

    def create_user(self, email, password, **kwargs):
        user = self.model(
            email=self.normalize_email(email),
            is_active=True,
            **kwargs
        )
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_superuser(self, email, password, **kwargs):
        user = self.model(
            email=email,
            is_staff=True,
            is_superuser=True,
            is_active=True,
            **kwargs
        )
        user.set_password(password)
        user.save(using=self._db)
        return user


class User(AbstractBaseUser, PermissionsMixin):
    class Meta:
        ordering = ('first_name', 'last_name')

    objects = UserManager()
    # username = models.CharField(_('username'), max_length=300)
    email = models.EmailField(unique=True)
    civility = models.CharField(
        max_length=8,
        choices=accounts.static_data.CIVILITIES,
        null=True,
        blank=True,
        verbose_name=_('civility')
    )
    first_name = models.CharField(_('first name'), max_length=300)
    last_name = models.CharField(_('last name'), max_length=300)

    is_staff = models.BooleanField(
        _('staff status'),
        default=False,
        help_text=_('admin ?')
    )
    is_active = models.BooleanField(
        _('active'),
        default=True,
        help_text=_('Active user ?')
    )
    date_joined = models.DateTimeField(_('date joined'), default=timezone.now)

    primary_group = models.ForeignKey(
        'accounts.Group',
        verbose_name=_('Groupe principal'),
        help_text=_("Groupe principal. Il doit être défini si l'utilisateur appartient à plusieurs groupes."),
        on_delete=models.SET_NULL,
        blank=True,
        null=True,
        related_name='primary_group'
    )

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = ['first_name', 'last_name']

    def get_short_name(self):
        return self.email

    def get_groups(self):
        if self.primary_group is not None:
            groups = [self.primary_group]
        else:
            groups = []

        for g in Group.objects.all():
            if g.is_user_in(self) and g not in groups:
                groups.append(g)
        return groups

    @property
    def name(self):
        return self.username

    def get_full_name(self):
        return f'{self.first_name} {self.last_name}'

    def __str__(self):
        return f'{self.first_name} {self.last_name} ({self.email})'


class Group(models.Model):
    """
    """
    class Meta:
        verbose_name = _('Groupe')
        verbose_name_plural = _('Groupes')

    name = models.CharField(
        _("Nom"),
        blank=False,
        max_length=50,
    )
    user = models.ManyToManyField(
        User,
        blank=True,
        verbose_name=_('Contact'),
    )
    domainname = models.CharField(
        _("Nom de domaine"),
        blank=True,
        max_length=50,
        help_text=_('Tous les utilisateurs dont l\'email se termine par ce nom de domaine seront compris dans ce groupe.'),
    )

    def __str__(self):
        return ' - '.join(
            [
                str(self.name),
                str(self.domainname),
            ]
        )

    def get_users(self):
        all_users = list(self.user.all())
        for u in User.objects.all():
            if u.email.endswith(self.domainname) and u not in all_users:
                all_users.append(u)

        return all_users

    def is_user_in(self, user):
        return user in self.get_users()
