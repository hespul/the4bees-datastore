# -*- coding: utf-8 -*-
from django.test import TestCase
from django.template.defaultfilters import slugify

from django.conf import settings

from .models import User, Group

import faker
from factory import DjangoModelFactory, lazy_attribute


fake = faker.Factory.create('fr_FR')


class UserFactory(DjangoModelFactory):
    class Meta:
        model = 'accounts.User'
    first_name = lazy_attribute(lambda o: fake.first_name())
    last_name = lazy_attribute(lambda o: fake.last_name())
    username = lazy_attribute(lambda o: '{0}.{1}@{2}'.format(
        o.first_name,
        o.last_name,
        fake.domain_name()
    ).lower().replace(' ', '-').encode('ascii', 'ignore'))
    email = username


class GroupFactory(DjangoModelFactory):
    class Meta:
        model = 'accounts.Group'

    name = lazy_attribute(lambda o: fake.name())



class GroupTestCase(TestCase):
    def setUp(self):
        self.contact1 = UserFactory()
        self.contact1.username = '@'.join([str(self.contact1.username[:7]).split('@')[0], str('example.com')])
        self.contact1.email = self.contact1.username
        self.contact1.save()
        self.contact2 = UserFactory()
        self.contact2.username = '@'.join([str(self.contact2.username[:7]).split('@')[0], str('example.com')])
        self.contact2.email = self.contact2.username
        self.contact2.save()
        self.contact3 = UserFactory()
        self.contact3.username = '@'.join([str(self.contact3.username[:7]).split('@')[0], str('example.com')])
        self.contact3.email = self.contact3.username
        self.contact3.save()
        self.contact4 = UserFactory()
        self.contact4.username = '@'.join([str(self.contact4.username[:7]).split('@')[0], str('anotherexample.com')])
        self.contact4.email = self.contact4.username
        self.contact4.save()
        self.contact5 = UserFactory()
        self.contact5.username = '@'.join([str(self.contact5.username[:7]).split('@')[0], str('anotherexample.com')])
        self.contact5.email = self.contact5.username
        self.contact5.save()

        self.group = GroupFactory()
        self.group.domainname = '@example.com'
        self.group.user.add(self.contact5)
        self.group.save()

    def test_user_groups(self):
        self.assertIn(
            self.group,
            self.contact1.get_groups()
        )
        self.assertNotIn(
            self.group,
            self.contact4.get_groups()
        )
        self.assertIn(
            self.group,
            self.contact5.get_groups()
        )

    def test_group_content(self):
        self.assertIn(
            self.contact1,
            self.group.get_users()
        )
        self.assertIn(
            self.contact2,
            self.group.get_users()
        )
        self.assertIn(
            self.contact3,
            self.group.get_users()
        )
        self.assertNotIn(
            self.contact4,
            self.group.get_users()
        )
        self.assertIn(
            self.contact5,
            self.group.get_users()
        )
        self.assertTrue(
            self.group.is_user_in(self.contact1)
        )
        self.assertTrue(
            self.group.is_user_in(self.contact2)
        )
        self.assertTrue(
            self.group.is_user_in(self.contact3)
        )
        self.assertFalse(
            self.group.is_user_in(self.contact4)
        )
        self.assertTrue(
            self.group.is_user_in(self.contact5)
        )
