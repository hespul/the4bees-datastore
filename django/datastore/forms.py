# -*- coding: utf-8 -*-
from django import forms

from .models import SensorBox


class SensorBoxWizardForm(forms.ModelForm):
    class Meta:
        model = SensorBox
        exclude = []

