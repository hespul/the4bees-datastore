# -*- coding: utf-8 -*-
from django.conf import settings


def tag_app_version_middleware(get_response):
    """
    Middleware for setting a cookie 'app_version' 
    containing APP_VERSION environment variable content
    """
    def middleware(request):
        # Code to be executed for each request before
        # the view (and later middleware) are called.

        response = get_response(request)

        response.set_cookie('app_version',
                            settings.APP_VERSION,
                            max_age=settings.CSRF_COOKIE_AGE)

        return response

    return middleware
