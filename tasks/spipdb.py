from invoke import task

from tasks.docker import dc



# DATABASE ################################################################################
    
@task(help={
    'service': 'service name [djcms]',
})
def list(ctx, service=None):
    """
    List available backups for service
    """
    service = service or ctx.service
    dc(ctx, service, 'run --rm mysql list-backups')


@task(help={
    'service': 'service name [djcms]',
})
def shell(ctx, service=None):
    """
    Get a psql shell for service
    """
    service = service or ctx.service
    dc(ctx, service, "exec mysql bash -c 'psql -U $MYSQL_USER'", pty=True)


@task(help={
    'service': 'service name [djcms]',
    'name': 'file to restore',
})
def restore(ctx, name, service=None):
    """
    Restore database for [service] from file [name]
    """
    service = service or ctx.service
    # check running mysql    
    dc(ctx, service, "ps |grep -e 'mysql.*Up'".format(name))

    from tasks.docker import down, upd

    down(ctx, service, 'django')
    dc(ctx, service, 'run --rm mysql restore {}'.format(name))
    upd(ctx, service, 'django')


@task(help={
    'service': 'service name [djcms]',
    'name': 'filename to save data to',
})
def backup(ctx, name=None, service=None):
    """
    Save database for [service] in file [name]
    """
    service = service or ctx.service
    name = name or ctx.service

    # check running mysql    
    dc(ctx, service, "ps |grep -e 'mysql.*Up'".format(name))
    
    dc(ctx, service, 'run --rm mysql backup {}'.format(name))

