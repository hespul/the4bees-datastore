# -*- coding: utf-8 -*-
import environ
import glob
import os
import sys


# ## Logging ##################################

LOGGING = {
    "version": 1,
    "disable_existing_loggers": False,
    'handlers': {
        'console': {
            'level': 'INFO',
            'class': 'logging.StreamHandler',
            'stream': sys.stdout
        },
    },
    'loggers': {
        'django': {
            'level': 'ERROR',
            'handlers': ['console'],
        },
        'accounts': {
            'level': 'ERROR',
            'handlers': ['console'],
        },
        'articles_layout': {
            'level': 'ERROR',
            'handlers': ['console'],
        },
        'config': {
            'level': 'ERROR',
            'handlers': ['console'],
        },
        'custom_http_errors': {
            'level': 'ERROR',
            'handlers': ['console'],
        },
        'experiences': {
            'level': 'ERROR',
            'handlers': ['console'],
        },
        'fac': {
            'level': 'ERROR',
            'handlers': ['console'],
        },
        'foundation': {
            'level': 'ERROR',
            'handlers': ['console'],
        },
        'news': {
            'level': 'ERROR',
            'handlers': ['console'],
        },
        'photovoltaique_info': {
            'level': 'ERROR',
            'handlers': ['console'],
        },
        'submissions': {
            'level': 'ERROR',
            'handlers': ['console'],
        },
    },
}

# ## /Logging ##################################
