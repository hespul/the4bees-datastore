from rest_framework import viewsets
from rest_framework.response import Response
from helpers.serializers import TuppleSerializer


class ChoiceViewsetMixin(viewsets.ViewSet):
    """
    Serialize CHOICES tupple
    """
    choice = None

    def list(self, request):
        items = []
        for id, value in self.choice:
            items.append(
                {
                    'id': id,
                    'value': value,
                }
            )
        serializer = TuppleSerializer(items, many=True)
        return Response(serializer.data)
