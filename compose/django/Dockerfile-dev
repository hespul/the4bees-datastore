FROM python:3.6
MAINTAINER Alexandre Norman <alexandre.norman@hespul.org>

ENV PYTHONUNBUFFERED 1

RUN mkdir /root/.ssh && \
    chmod 700 -R /root/.ssh/ && \
    groupadd -r django && \
    useradd -r -g django django

# Local user for dev
RUN groupadd -r docker -g 1000 && \
    useradd -u 1000 -r -g docker -d /app -s /sbin/nologin -c "Docker image user" docker && \
    mkdir /pipcache

RUN apt-get update && \
    apt-get upgrade -y && \
    apt-get install -y gettext && \
    apt-get clean && \
    apt-get autoclean

COPY ./compose/django/entrypoint.sh /entrypoint.sh
COPY ./compose/django/start-dev.sh /start-dev.sh
COPY ./compose/django/start-background-tasks-worker.sh /start-background-tasks-worker.sh
COPY ./compose/django/pip-compile.sh /pip-compile.sh
RUN chmod +x /entrypoint.sh /start-dev.sh /start-background-tasks-worker.sh /pip-compile.sh

RUN mkdir -p /media && chmod 777 /media

RUN pip install --upgrade pip

# Requirements have to be pulled and installed here, otherwise caching won't work
COPY ./django/requirements/production.txt  /requirements.txt
RUN pip install -r /requirements.txt

COPY ./django/requirements/development.txt /requirements-dev.txt
RUN pip install -r /requirements-dev.txt

# Clean up to make docker image smaller
RUN rm -rf /tmp/* /var/tmp/*  && \
    rm -rf /usr/share/doc && \
    rm -rf /root/.ssh/ && \
    rm -rf /pipcache && \
    rm -rf /requirements && \
    apt-get clean && apt-get autoclean


WORKDIR /app/django

RUN mkdir -p /app/.cache && chmod 777 -R /app/.cache


ARG APP_VERSION='development'
ENV APP_VERSION=$APP_VERSION

ENTRYPOINT ["/entrypoint.sh"]
