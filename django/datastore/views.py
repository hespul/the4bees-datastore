# -*- coding: utf-8 -*-
from django.shortcuts import get_object_or_404
from rest_framework import viewsets
from rest_framework.response import Response
from rest_framework.decorators import api_view, permission_classes
from rest_framework.permissions import AllowAny

from django.views.generic.base import View

from .models import (
    DataPoint,
    Message,
)

from .serializers import (
    DataPointSerializer,
    DataPointAverageSerializer,
    InputDataPointSerializer,
    MessageSerializer,
)

from csv_export.views import CSVExportView


class MixinFilterDataPointQueryset():
    def filtering_get_queryset(self, queryset):
        """
        http://172.16.1.7:8000/datastore/datapoint/?sensor_box_slug=hespul-educ-sensib&start_date=2018-08-30&end_date=2018-08-30&limit=10
        """
        sensor_box_slug = self.get_value_from_query('sensor_box_slug')
        if sensor_box_slug:
            queryset = queryset.filter(sensor_box__slug=sensor_box_slug)

        start_date = self.get_value_from_query('start_date')
        if start_date:
            queryset = queryset.filter(date__date__gte=start_date)

        end_date = self.get_value_from_query('end_date')
        if end_date:
            queryset = queryset.filter(date__date__lte=end_date)
        return queryset

    def limiting_get_queryset(self, queryset):
        limit = self.get_value_from_query('limit')

        if limit:
            queryset = queryset[:int(limit)]

        return queryset

    def get_value_from_query(self, key):
        try:
            return self.request.GET[key]
        except KeyError:
            return None


class MixinForDrfFilterDataPointQueryset(MixinFilterDataPointQueryset):
    def get_value_from_query(self, key):
        return self.request.query_params.get(key)


@permission_classes((AllowAny, ))
class DataPointAsCSV(MixinFilterDataPointQueryset, CSVExportView):
    """
    http://172.16.1.7:8000/datastore/datapoint-as-csv?sensor_box_slug=hespul-educ-sensib&start_date=2018-08-30&end_date=2018-08-30&limit=10
    """
    model = DataPoint
    specify_separator = False
    fields = (
        'date',
        'temperature',
        'voc',
        'co2',
        'humidity',
        'lux',
        'sensor_box_slug',
    )

    def get_queryset(self):
        qs = DataPoint.objects.select_related("sensor_box").all().order_by('-date')
        qs = self.filtering_get_queryset(qs)
        qs = self.limiting_get_queryset(qs)
        return qs


@permission_classes((AllowAny, ))
class DataPointViewSet(MixinForDrfFilterDataPointQueryset, viewsets.ModelViewSet):
    """
    API endpoint that allows DataPoint to be viewed or edited.
    """
    serializer_class = DataPointSerializer
    queryset = DataPoint.objects.all().order_by('-date')

    def get_queryset(self):
        qs = DataPoint.objects.all().order_by('-date')
        qs = self.filtering_get_queryset(qs)
        qs = self.limiting_get_queryset(qs)
        return qs


@permission_classes((AllowAny, ))
class DataPointDailyAverageViewSet(MixinForDrfFilterDataPointQueryset, viewsets.ViewSet):
    """
    API endpoint that allows DataPoint Average to be viewed

    http://172.16.1.7:8000/datastore/datapoint-avg-day/?sensor_box_slug=hespul-educ-sensib&start_date=2018-08-29&end_date=2018-08-30&limit=2
    """
    def get_queryset(self):
        qs = DataPoint.objects.all().order_by('-date')
        qs = self.filtering_get_queryset(qs)
        return qs

    def list(self, request):
        queryset = self.get_queryset()
        all_data = {}
        for data in queryset:
            date = data.date.date()
            if date not in all_data:
                all_data[date] = {
                    "temperature": [data.temperature],
                    "voc": [data.voc],
                    "co2": [data.co2],
                    "humidity": [data.humidity],
                    "lux": [data.lux],
                }
            else:
                all_data[date]['temperature'].append(data.temperature)
                all_data[date]['voc'].append(data.voc)
                all_data[date]['co2'].append(data.co2)
                all_data[date]['humidity'].append(data.humidity)
                all_data[date]['lux'].append(data.lux)

        avg = []
        for date in all_data:
            avg.append(
                {
                    'date': date,
                    'temperature': round(sum(all_data[date]['temperature'])/float(len(all_data[date]['temperature'])), 1),
                    'voc': round(sum(all_data[date]['voc'])/float(len(all_data[date]['voc'])), 0),
                    'co2': round(sum(all_data[date]['co2'])/float(len(all_data[date]['co2'])), 0),
                    'humidity': round(sum(all_data[date]['humidity'])/float(len(all_data[date]['humidity'])), 1),
                    'lux': round(sum(all_data[date]['lux'])/float(len(all_data[date]['lux'])), 0),
                }
            )

        limit = request.query_params.get('limit')
        if limit:
            avg = avg[:int(limit)]

        serializer = DataPointAverageSerializer(avg, many=True)
        return Response(serializer.data)


@permission_classes((AllowAny, ))
class InputDataPointViewSet(viewsets.ViewSet):
    """
    A simple ViewSet for storing data points
    {
        "temperature": 123.0,
        "voc": 1209.0,
        "co2": 120.0,
        "humidity": 80.0,
        "lux": 109.0,
        "sensor_box_slug": "hespul-educ-sensib",
        "key": "8baf3826-0935-456b-ae0c-f5538bf1e4d8"
    }
    """
    def get_queryset(self):
        queryset = self.queryset.all()  # Force re-evaluation of self.queryset because DRF is caching
        return queryset

    def list(self, request):
        queryset = DataPoint.objects.all().order_by('-date')
        serializer = DataPointSerializer(queryset, many=True)
        return Response(serializer.data)

    def retrieve(self, request, pk=None):
        queryset = DataPoint.objects.all().order_by('-date')
        data_point = get_object_or_404(queryset, pk=pk)
        serializer = DataPointSerializer(data_point)
        return Response(serializer.data)

    def create(self, request):
        data = request.data

        serializer = InputDataPointSerializer(data=data)
        if serializer.is_valid(raise_exception=True):
            obj = serializer.save()
            obj.save()

        return Response(
            DataPointSerializer(obj).data
        )


@permission_classes((AllowAny, ))
class MessageListViewSet(viewsets.ViewSet):
    """
    List Message
    """
    def get_value_from_query(self, key):
        try:
            return self.request.GET[key]
        except KeyError:
            return None

    def filtering_get_queryset(self, queryset):
        """
        Filter queryset according "sensor_box_slug" url parameter
        """
        sensor_box_slug = self.get_value_from_query('sensor_box_slug')
        if sensor_box_slug:
            filtered_qs = queryset.filter(sensor_box__slug=sensor_box_slug)

        # check that there is a rule for each mesure
        mesures = [x for x in filtered_qs]
        for mesure_type in [
            "temperature",
            "humidity",
            "lux",
            "co2",
            "voc",
        ]:
            if not filtered_qs.filter(mesure=mesure_type).exists():
                try:
                    mesures.append(
                        queryset.filter(mesure=mesure_type, start_date=None, end_date=None).first().pk
                    )
                except AttributeError:
                    pass

        return queryset.filter(id__in=mesures)

    def get_queryset(self):
        qs = Message.objects.all()
        # qs = self.filtering_get_queryset(qs)
        return qs

    def list(self, request):
        queryset = self.get_queryset()
        serializer = MessageSerializer(queryset, many=True)
        return Response(serializer.data)
