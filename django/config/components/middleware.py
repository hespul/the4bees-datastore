# -*- coding: utf-8 -*-

from .components.base import ALLOWED_HOSTS

# ## Middleware ##################################

MIDDLEWARE = [
]

# if '*' in ALLOWED_HOSTS:
#     logger.warning(f"'*' in ALLOWED_HOSTS  => allowed_hosts.middleware.check_allowed_hosts IS ENABLED")
#     MIDDLEWARE += [
#         'allowed_hosts.middleware.check_allowed_hosts',
#     ]
# else:
#     logger.warning(f"'*' not in ALLOWED_HOSTS ({ALLOWED_HOSTS}) => allowed_hosts.middleware.check_allowed_hosts is NOT enabled")

MIDDLEWARE += [
    'django.contrib.sessions.middleware.SessionMiddleware',
    # 'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.locale.LocaleMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.contrib.sites.middleware.CurrentSiteMiddleware',
    'corsheaders.middleware.CorsMiddleware',
]


# ## /Middleware ##################################
