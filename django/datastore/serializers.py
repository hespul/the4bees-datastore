# -*- coding: utf-8 -*-
from rest_framework import serializers
from django.utils import timezone

from .models import (
    DataPoint,
    Message,
    SensorBox,
)


class DateTimeFieldWihTZ(serializers.DateTimeField):
    '''
    Class to make output of a DateTime Field timezone aware
    (from https://fixes.co.za/django/make-django-rest-framework-date-time-fields-timezone-aware/)
    '''
    def to_representation(self, value):
        value = timezone.localtime(value)
        return super(DateTimeFieldWihTZ, self).to_representation(value)


class DataPointSerializer(serializers.ModelSerializer):

    class Meta:
        model = DataPoint
        fields = (
            'id',
            'date',
            'temperature',
            'voc',
            'co2',
            'humidity',
            'lux',
            'sensor_box',
        )

    date = DateTimeFieldWihTZ(format='%Y-%m-%dT%H:%M:%S.%f+00:00')


class DataPointAverageSerializer(serializers.Serializer):
    date = serializers.DateField()
    temperature = serializers.FloatField()
    voc = serializers.FloatField()
    co2 = serializers.FloatField()
    humidity = serializers.FloatField()
    lux = serializers.FloatField()


class InputDataPointSerializer(serializers.Serializer):
    temperature = serializers.FloatField()
    voc = serializers.FloatField()
    co2 = serializers.FloatField()
    humidity = serializers.FloatField()
    lux = serializers.FloatField()
    sensor_box_slug = serializers.CharField(max_length=128)
    key = serializers.CharField(max_length=128)

    def validate(self, data):
        """
        Check the sensor box key
        """
        if not SensorBox.objects.filter(
                slug=data['sensor_box_slug'],
                uuid=data['key'],
        ).exists():
            raise serializers.ValidationError("No valid sensor box found")
        return data

    def create(self, validated_data):
        return DataPoint(
            temperature=validated_data['temperature'],
            voc=validated_data['voc'],
            co2=validated_data['co2'],
            humidity=validated_data['humidity'],
            lux=validated_data['lux'],
            sensor_box=SensorBox.objects.get(
                slug=validated_data['sensor_box_slug'],
            ),
        )

    def update(self, instance, validated_data):
        raise serializers.ValidationError("You can't update a datapoint")


class MessageSerializer(serializers.ModelSerializer):
    icon = serializers.CharField(source='fa_icon')

    class Meta:
        model = Message
        fields = (
            'id',
            'mesure',
            'title',
            'short_message',
            'message',
            'min_value',
            'max_value',
            'start_date',
            'end_date',
            'icon',
            'icon_color',
        )
