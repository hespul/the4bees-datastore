# -*- coding: utf-8 -*-
import logging
logger = logging.getLogger(__name__)  # NOQA

from django.db import models
from django.utils.translation import ugettext_lazy as _
from helpers.models import representation_helper

from djangocms_text_ckeditor.fields import HTMLField
from django.urls import reverse
from colorful.fields import RGBColorField

from .sensor_box import SensorBox


@representation_helper
class Message(models.Model):
    class Meta:
        verbose_name = _('Message')
        ordering = ['pk']

    MESURE_TYPES = (
        ("temperature", _("Température")),
        ("humidity", _("Humidité")),
        ("lux", _("Lumière")),
        ("co2", _("CO2")),
        ("voc", _("COV")),
    )
    ICONS = (
        (0, _("unhappy")),
        (1, _("neutral")),
        (2, _("smile")),
        (3, _("sweaty")),
        (4, _("angry")),
    )
    ICONS_FA = {
        'unhappy': 'frown',
        'smile':   'smile-beam',
        'sweaty':  'grin-beam-sweat',
        'neutral': 'meh',
        'angry':   'angry',
    }
    mesure = models.CharField(
        _("Mesure"),
        max_length=20,
        choices=MESURE_TYPES,
    )
        
    title = models.CharField(
        _("Titre"),
        max_length=150,
    )
    sensor_box = models.ManyToManyField(
        SensorBox,
        blank=True,
        verbose_name=_("Sensor Box"),
        help_text=_("Si aucune sensor box n'est sélectionnée, cela s'applique pour toutes")
    )
    short_message = models.CharField(
        verbose_name=_("Message court affiché dans l'interface"),
        max_length=140,
        blank=False,
        null=False,
    )
    message = HTMLField(
        verbose_name=_("Message d'explications complet qui sera affiché à l'utilisateur"),
        blank=False,
        null=False,
    )
    start_date = models.DateField(
        verbose_name=_("Date de début d'application"),
        help_text=_('Le jour et le mois seront utilisés'),
        blank=True,
        null=True,
    )
    end_date = models.DateField(
        verbose_name=_("Date de fin d'application"),
        help_text=_('Le jour et le mois seront utilisés'),
        blank=True,
        null=True,
    )
    min_value = models.FloatField(
        null=True,
    )
    max_value = models.FloatField(
        null=True,
    )
    icon = models.IntegerField(
        default=0,
        choices=ICONS,
    )
    icon_color = RGBColorField(
        colors=['#FFAC1C', '#00972F', '#000', '#922790', '#F68C32', '#007EC5',
                '#8DB946', '#F68C32', '#4E6E8D'],
        verbose_name=_(_("Couleur de l'icône")),
        default='#000',
    )
    creation_date = models.DateTimeField(
        verbose_name=_("Date de création"),
        auto_now_add=True
    )
    last_modification = models.DateTimeField(
        verbose_name=_("Date de modification"),
        auto_now=True
    )

    @property
    def fa_icon(self):
        return self.ICONS_FA[self.get_icon_display()]
        
    def __str__(self):
        return f"{self.mesure} {self.title}"

    # def get_absolute_url(self):
    #     return reverse("admin:datastore_sensorbox_change", args=[str(self.pk)])
