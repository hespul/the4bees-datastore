#!/bin/bash

cd /app/django/requirements
pip-compile --annotate -U base.in -v -o  base.txt
pip-compile --annotate -U production.in -v -o  production.txt
pip-compile --annotate -U development.in -v -o  development.txt

