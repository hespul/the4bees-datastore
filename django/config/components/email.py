# -*- coding: utf-8 -*-
import environ
import glob
import os
import sys


# ## Email configuration ##################################

EMAIL_HOST = env('EMAIL_HOST', default='mail')
EMAIL_PORT = env('EMAIL_PORT', default=1025)
EMAIL_BACKEND = 'django.core.mail.backends.smtp.EmailBackend'
EMAIL_HOST_USER = env('EMAIL_HOST_USER', default='docker')
EMAIL_HOST_PASSWORD = env('EMAIL_HOST_PASSWORD', default='')
EMAIL_USE_TLS = False

DEFAULT_FROM_EMAIL = env('DJANGO_DEFAULT_FROM_EMAIL')
DEFAULT_FROM_EMAIL_NAME = env('DJANGO_DEFAULT_FROM_EMAIL_NAME', default='')

EMAIL_USE_TLS = env.bool('EMAIL_USE_TLS', default=False)
EMAIL_USE_SSL = env.bool('EMAIL_USE_SSL', default=False)

# Contact form
CONTACT_MAIL_TO = env.list('CONTACT_MAIL_TO')

# ## /Email configuration ##################################
