# -*- coding: utf-8 -*-
import unicodedata
import datetime
import locale

from django.utils.translation import ugettext_lazy as _


def remove_diacritics(value):
    """
    Return string without diacritics (Épice => Epice)
    """
    return ''.join((c for c in unicodedata.normalize('NFD', value) if unicodedata.category(c) != 'Mn'))


def format_datetime_interval(dt1, dt2, include_time=True):
    def get_time_format(dt):
        return dt.strftime("%H:%M")

    same_year = dt1.year == dt2.year
    same_month = dt1.month == dt2.month
    same_day = dt1.day == dt2.day
    current_year = (dt1.year == datetime.date.today().year and same_year)

    if include_time and (not isinstance(dt1, datetime.datetime) or not isinstance(dt2, datetime.datetime)):
        include_time = False

    # Force locale - REWORK NEEDED
    locale.setlocale(locale.LC_ALL, 'fr_FR.utf8')

    if same_year and same_month and same_day:
        if current_year:
            # formatted = format(dt1, 'd F')
            formatted = datetime.datetime.strftime(dt1, '%d %B')
        else:
            # formatted = format(dt1, 'd F Y')
            formatted = datetime.datetime.strftime(dt1, '%d %B %Y')
        if include_time:
            if dt1.hour == dt2.hour and dt1.minute == dt2.minute:
                return "{0}, {1}".format(formatted, get_time_format(dt1))
            else:
                return "{0}, {1} - {2}".format(formatted, get_time_format(dt1), get_time_format(dt2))
        else:
            return _('le {}').format(formatted)

    elif same_year and same_month and not include_time:
        if current_year:
            return 'du ' + dt1.strftime("%d") + datetime.datetime.strftime(dt2, ' au %d %B')
        else:
            return 'du ' + dt1.strftime("%d") + datetime.datetime.strftime(dt2, ' au %d %B %Y')

    elif same_year and not include_time:
        if current_year:
            return 'du ' + datetime.datetime.strftime(dt1, '%d %B') + datetime.datetime.strftime(dt2, ' au %d %B')
        else:
            return 'du ' + datetime.datetime.strftime(dt1, '%d %B') + datetime.datetime.strftime(dt2, ' au %d %B %Y')

    else:
        if include_time:
            if current_year:
                return "du {0}, {1} au {2}, {3}".format(
                    datetime.datetime.strftime(dt1, '%d %B'),
                    get_time_format(dt1),
                    datetime.datetime.strftime(dt2, '%d %B'),
                    get_time_format(dt2)
                )
            else:
                return "du {0}, {1} au {2}, {3}".format(
                    datetime.datetime.strftime(dt1, '%d %B %Y'),
                    get_time_format(dt1),
                    datetime.datetime.strftime(dt2, '%d %B %Y'),
                    get_time_format(dt2)
                )
        else:
            if current_year:
                return 'du ' + datetime.datetime.strftime(dt1, '%d %B %Y') + datetime.datetime.strftime(dt2, ' au %d %B')
            else:
                return 'du ' + datetime.datetime.strftime(dt1, '%d %B %Y') + datetime.datetime.strftime(dt2, ' au %d %B %Y')

