# -*- coding: utf-8 -*-
from django import template
from config.settings import APP_VERSION
import datetime

register = template.Library()

@register.filter(name='get_app_version')
def get_app_version(value):
    return APP_VERSION + str(datetime.datetime.now().timestamp()).split('.')[0][0:8]
