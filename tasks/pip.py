import os
from invoke import task

@task
def upgrade(ctx):
    """
    Upgrade pip dependencies by launching pip-compile
    """
    cmd = 'cd django/requirements; pip-compile --annotate -U base.in -v -o  base.txt'
    ctx.run(cmd)
    cmd = 'cd django/requirements; pip-compile --annotate -U production.in -v -o  production.txt'
    ctx.run(cmd)
    cmd = 'cd django/requirements; pip-compile --annotate -U development.in -v -o  development.txt'
    ctx.run(cmd)
