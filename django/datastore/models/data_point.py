# -*- coding: utf-8 -*-
import logging
logger = logging.getLogger(__name__)  # NOQA

from django.db import models
from django.utils.translation import ugettext_lazy as _
from helpers.models import representation_helper

from .sensor_box import SensorBox

"""
import random
import datetime
from datetime import timedelta
now = datetime.datetime.now()
s = SensorBox.objects.first()
for i in range(10000):
    DataPoint.objects.create(
        temperature=random.random() * 30,
        voc=random.random() * 400,
        co2=random.random() * 3000,
        humidity=random.random() * 100,
        lux=random.random() * 30000,
        sensor_box=s,
    )

for i, item in enumerate(DataPoint.objects.all()):
    item.date = now + timedelta(minutes=i)
    item.save()

"""


@representation_helper
class DataPoint(models.Model):
    class Meta:
        verbose_name = _('Data point')
        verbose_name_plural = _('Data points')
        ordering = ['date']

    date = models.DateTimeField(
        verbose_name=_('Date'),
        auto_now_add=True
    )
    temperature = models.FloatField(
        blank=False,
        null=False,
        default=0,
    )
    voc = models.FloatField(
        blank=False,
        null=False,
        default=0,
    )
    co2 = models.FloatField(
        blank=False,
        null=False,
        default=0,
    )
    humidity = models.FloatField(
        blank=False,
        null=False,
        default=0,
    )
    lux = models.FloatField(
        blank=False,
        null=False,
        default=0,
    )
    sensor_box = models.ForeignKey(
        SensorBox,
        blank=False,
        null=False,
        verbose_name=_('Sensor Box'),
        on_delete=models.CASCADE,
    )

    def __str__(self):
        return f'{self.sensor_box.slug} {self.date}'

    @property
    def sensor_box_slug(self):
        return self.sensor_box.slug
