# -*- coding: utf-8 -*-
from django.conf.urls import url

# from .views import AccountUpdate, RegistrationView
from django.contrib.auth import views as djviews
from config.settings import ALLOW_USER_SELF_REGISTRATION

import logging
logger = logging.getLogger(__name__)


urlpatterns = [
    # Gestion des comptes
    # url(
    #     r'^account/$',
    #     AccountUpdate.as_view(),
    #     name='account'
    # ),

    # Login / logout
    url(
        r'^login/$',
        djviews.login,
        name='login'
    ),
    url(
        r'^logout/$',
        djviews.logout,
        {
            'next_page': 'accounts:login'
        },
        name='logout'
    ),

    # Changement de mot de passe manuel
    url(
        r'^password_change/$',
        djviews.password_change,
        {
            'template_name': 'password_change_form.html',
            'post_change_redirect': 'accounts:password_change_done'
        },
        name='password_change'
    ),
    url(
        r'^password_change/done/$',
        djviews.password_change_done,
        {
            'template_name': 'password_change_done.html'
        },
        name='password_change_done'
    ),

    # Récupération de mot de passe par mail
    url(
        r'^password/reset/$',
        djviews.password_reset,
        {
            'post_reset_redirect': 'accounts:password_reset_done',
            'email_template_name': 'registration/password_reset_email-fac.txt',
            'html_email_template_name': 'registration/password_reset_email-fac.html',
            'post_reset_redirect': 'accounts:password_reset_done',
        },
        name="password_reset",
    ),
    url(
        r'^password/reset/done/$',
        djviews.password_reset_done,
        name='password_reset_done'
    ),
    url(
        r'^password/reset/(?P<uidb64>[0-9A-Za-z_\-]+)/(?P<token>[0-9A-Za-z]{1,13}-[0-9A-Za-z]{1,20})/$',
        djviews.password_reset_confirm,
        {
            'post_reset_redirect': 'accounts:password_reset_complete'
        },
        name="password_reset_confirm"
    ),
    url(
        r'^password/done/$',
        djviews.password_reset_complete,
        name='password_reset_complete'
    ),
]


# if ALLOW_USER_SELF_REGISTRATION:
#     logger.warning('ALLOW_USER_SELF_REGISTRATION turned ON')
# 
#     urlpatterns += [
#         # inscription utilisateur
#         # rendre conditionnel settings
#         url(
#             r'^register/$',
#             RegistrationView.as_view(),
#             {
#                 'template_name': 'registration/register.html',
#             },
#             name='register'
#         ),
#         url(
#             r'^register/done/$',
#             djviews.password_reset_done,
#             {
#                 'template_name': 'registration/initial_done.html',
#             },
#             name='register-done'
#         ),
#         url(
#             r'^register/password/(?P<uidb64>[0-9A-Za-z_\-]+)/(?P<token>[0-9A-Za-z]{1,13}-[0-9A-Za-z]{1,20})/$',
#             djviews.password_reset_confirm,
#             {
#                 'template_name': 'registration/initial_confirm.html',
#                 'post_reset_redirect': 'accounts:register-complete'
#             },
#             name='register-confirm'
#         ),
#         url(
#             r'^register/complete/$',
#             djviews.password_reset_complete,
#             {
#                 'template_name': 'registration/initial_complete.html'
#             },
#             name='register-complete'
#         ),
#     ]


logger.debug('{0}'.format(urlpatterns))
