# -*- coding: utf-8 -*-
import logging
logger = logging.getLogger(__name__)  # NOQA

from django.db import models
from django.utils.translation import ugettext_lazy as _
from helpers.models import representation_helper

from djangocms_text_ckeditor.fields import HTMLField
from django.urls import reverse

from accounts.models import (
    User,
)

import uuid


@representation_helper
class SensorBox(models.Model):
    class Meta:
        verbose_name = _('SensorBox')
        ordering = ['pk']

    title = models.CharField(
        _('Titre'),
        max_length=150,
    )
    slug = models.SlugField(
        max_length=128,
        unique=True,
        help_text=_("Identifiant URL")
    )
    description = HTMLField(
        verbose_name=_('Description'),
        blank=False,
        null=False,
    )
    uuid = models.UUIDField(
        verbose_name=_('UUID'),
        unique=True,
        default=uuid.uuid4,
        editable=False,
    )
    is_public = models.BooleanField(
        verbose_name=_('Données publiques'),
        default=False,
    )
    owners = models.ManyToManyField(
        User,
        verbose_name=_('Propriétaires'),
        default=None,
        related_name="sensor_boxes",
        blank=True,
    )
    creation_date = models.DateTimeField(
        verbose_name=_('Date de création'),
        auto_now_add=True
    )
    last_modification = models.DateTimeField(
        verbose_name=_('Date de modification'),
        auto_now=True
    )

    def __str__(self):
        return f'{self.slug}'

    def get_absolute_url(self):
        return reverse('admin:datastore_sensorbox_change', args=[str(self.pk)])
