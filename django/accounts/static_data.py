from django.utils.translation import ugettext_lazy as _


CIVILITIES = (
    ('M.', _('M.')),
    ('Mme', _('Mme')),
    ('Mlle', _('Mlle')),
)
