# -*- coding: utf-8 -*-

import unicodedata


def remove_diacritics(s):
    """
    Return string without diacritics (Épice => Epice)
    """
    return ''.join((c for c in unicodedata.normalize('NFD', s) if unicodedata.category(c) != 'Mn'))
