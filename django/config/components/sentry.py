# -*- coding: utf-8 -*-
import environ
import logging

env = environ.Env()

logger = logging.getLogger(__name__)


# ## SENTRY  ##################################

SENTRY_KEY = env("SENTRY_KEY", default=None)
SENTRY_PROJECT = env("SENTRY_PROJECT", default=None)
SENTRY_HOST = env("SENTRY_HOST", default="sentry.hespul.org")

if SENTRY_KEY and SENTRY_PROJECT and SENTRY_HOST and not DEBUG:  # NOQA
    logger.warning("Adding Sentry support (RAVEN).")
    INSTALLED_APPS += ["raven.contrib.django.raven_compat"]  # NOQA

    APP_VERSION = env("APP_VERSION", default="unknown")
    INSTANCE = env("DJANGO_SITE_URL", default="unknown")

    RAVEN_CONFIG = {
        "dsn": f"https://{SENTRY_KEY}@{SENTRY_HOST}/{SENTRY_PROJECT}",
        # If you are using git, you can also automatically configure the
        # release based on the git info.
        "release": APP_VERSION,
        "site": INSTANCE,
        "environment": env("APP_ENV", default="PROD"),
    }
else:
    logger.warning("Sentry (RAVEN) not enabled")

# ## /SENTRY  ##################################
