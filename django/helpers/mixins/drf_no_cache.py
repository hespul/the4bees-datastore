# -*- coding: utf-8 -*-


class DrfNoCache():
    def get_queryset(self):
        """
        Force re-evaluation of self.queryset because DRF is caching
        """
        queryset = self.queryset.all()
        return queryset
