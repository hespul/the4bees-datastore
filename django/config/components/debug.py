# -*- coding: utf-8 -*-
import environ
import glob
import os
import sys


import logging
logger = logging.getLogger(__name__)


env = environ.Env()

# SECURITY WARNING: don't run with debug turned on in production!

DEBUG = env.bool('DJANGO_DEBUG', default=False)
DEBUG_TOOLBAR = env.bool("DJANGO_DEBUG_TOOLBAR", False)


# ## DEBUG  ##################################

if DEBUG:
    SHELL_PLUS_PRINT_SQL = True
    SHELL_PLUS_SQLPARSE_FORMAT_KWARGS = dict(
        reindent_aligned=True,
        truncate_strings=500,
    )
    SHELL_PLUS = "ipython"


    MIDDLEWARE += [
        'querycount.middleware.QueryCountMiddleware',
    ]
    QUERYCOUNT = {
        'DISPLAY_DUPLICATES': 5,
    }

    
if DEBUG_TOOLBAR:

    INSTALLED_APPS += (
        'debug_toolbar',
    )
    
    MIDDLEWARE += [
        'debug_toolbar.middleware.DebugToolbarMiddleware',
    ]

    DEBUG_TOOLBAR_PATCH_SETTINGS = False
    DEBUG_TOOLBAR_PANELS = [
        'debug_toolbar.panels.versions.VersionsPanel',
        'debug_toolbar.panels.timer.TimerPanel',
        'debug_toolbar.panels.settings.SettingsPanel',
        'debug_toolbar.panels.headers.HeadersPanel',
        'debug_toolbar.panels.request.RequestPanel',
        'debug_toolbar.panels.sql.SQLPanel',
        'debug_toolbar.panels.staticfiles.StaticFilesPanel',
        'debug_toolbar.panels.templates.TemplatesPanel',
        'debug_toolbar.panels.cache.CachePanel',
        'debug_toolbar.panels.signals.SignalsPanel',
        'debug_toolbar.panels.logging.LoggingPanel',
        'debug_toolbar.panels.redirects.RedirectsPanel',
    ]

    def show_toolbar(request):
        return True

    DEBUG_TOOLBAR_CONFIG = {
        'DISABLE_PANELS': [
            'debug_toolbar.panels.redirects.RedirectsPanel',
        ],
        'SHOW_TEMPLATE_CONTEXT': True,
        'SHOW_TOOLBAR_CALLBACK': 'config.settings.show_toolbar',
    }


    # Set debug level for all
    for app in LOGGING['loggers']:
        LOGGING['loggers'][app]['level'] = 'DEBUG'

    
# ## /DEBUG  ##################################
