# -*- coding: utf-8 -*-

from django.core.management.base import BaseCommand
from django.contrib.sites.models import Site


class Command(BaseCommand):
    """
    """
    help = 'Set site URL'

    def add_arguments(self, parser):
        # Positional arguments
        parser.add_argument('site_url', type=str)

    def handle(self, *args, **options):
        s = Site.objects.get(pk=1)
        s.domain = options['site_url']
        s.save()
        self.stdout.write('Changed site domain to "{}"'.format(options['site_url']))
        return
