from invoke import Collection

import tasks.database
import tasks.spipdb
import tasks.docker
import tasks.django
import tasks.build
import tasks.test
import tasks.pip


ns = Collection(
    database,
    spipdb,
    docker,
    docker.up,
    docker.upd,
    docker.down,
    docker.restart,
    django,
    django.c,
    django.migrations,
    django.migrate,
    django.run,
    build,
    test.test,
    pip,
)


def get_project_name():
    import os
    try:
        return os.getcwd().split(os.path.sep)[-1]
    except:
        return None


ns.configure(
    {
        'service':  get_project_name() or 'djcms',
        'cont': '',
        'registry': 'registry.hespul.org:5000',
        'path_to_compose': 'compose/envs/',
        'domain': 'example.fr',
    }
)
