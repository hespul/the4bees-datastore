

class NestedAsRelatedMixin():
    """
    Mixin to apply on a ModelSerializer which take nested fields defined
    in the "nested_as_related_fields" and save items as ManyToMany using instance.set([id1, id2])
    To do that, it only use ids in the nested serializer items
    """

    def __set_many__(self, instance, field, value):
        field = getattr(instance, field)
        field.set(value)


    
    def __remove_nested_fields_from_validated_data__(self, validated_data):
        """
        For each registerd nested fields remove it from validated_data.
        Used overwise DRF will notice us it can't save related serializer
        """
        for nested_field_name in self.get_nested_as_related_fields():
            del validated_data[nested_field_name]

    def __save_nested_fields_many_to_many__(self, master):
        """
        for each registered nested field, fetch related items from database using a set of id field
        then associate these items with master as a ManyToMany
        """
        for nested_field_name in self.get_nested_as_related_fields():
            nested_field = self.get_fields().get(nested_field_name, None)
            if nested_field:
                nested_field_model = nested_field.child.Meta.model
                nested_field_item_ids = [n.get('id') for n in self.initial_data.get(nested_field_name, {})]
                nested_field_items = nested_field_model.objects.filter(id__in=nested_field_item_ids)
                self.__set_many__(master, nested_field_name, nested_field_items)

    def update(self, instance, validated_data, *args, **kwargs):
        self.__remove_nested_fields_from_validated_data__(validated_data)
        master = super(NestedAsRelatedMixin, self).update(instance, validated_data, *args, **kwargs)
        self.__save_nested_fields_many_to_many__(master)
        return master

    def create(self, validated_data, *args, **kwargs):
        self.__remove_nested_fields_from_validated_data__(validated_data)
        master = super(NestedAsRelatedMixin, self).create(validated_data, *args, **kwargs)
        self.__save_nested_fields_many_to_many__(master)
        return master

    def is_valid(self, *args, **kwargs):
        """
        Because DRF perform validation of nested serializer, we force it to only validate ids.
        1. We delete all other fields from initial_data (keep only id field)
        2. We set partial as True, so it will not complain about missing fields
        """
        original_partial = self.partial
        self.partial = True
        for nested_field_name in self.get_nested_as_related_fields():
            items = self.initial_data.pop(nested_field_name, None)
            new_items = []
            for item in items:
                new_items.append({'id': item['id']})
            self.initial_data[nested_field_name] = new_items

        ret = super(NestedAsRelatedMixin, self).is_valid(*args, **kwargs)
        self.partial = original_partial
        return ret

    def get_nested_as_related_fields(self):
        return getattr(self.Meta, 'nested_as_related_fields', ())
