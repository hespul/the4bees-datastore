from invoke import task

from tasks.docker import dc
from tasks.django import __run as django_run 

# BUILD ################################################################################


@task(help={
    'service': 'service name [djcms]',
    'container': 'container [all]',
    'force': 'force [False]'
})
def container(ctx, service=None, container='', force=False):
    """
    Build service container
    """
    service = service or ctx.service
    dc(
        ctx, service=service, command='build {} {}'.format(
            '--force-rm' * force,
            container
        )
    )


@task
def base(ctx):
    """
    Build base image service for django container and push it to registry
    """
    ctx.run('docker build -t djcms_django-base:latest -f compose/django/Dockerfile-base .')
    ctx.run('docker tag  djcms_django-base:latest {}/djcms_django-base:latest'.format(ctx.registry))
    ctx.run('docker push {}/djcms_django-base:latest'.format(ctx.registry))


@task(help={
    'service': 'service name [djcms]',
    'force': 'force [False]'
})
def prod(ctx, service, force=False):
    """
    Build prod images
    """
    ctx.run('docker-compose -p acproddjcms -f compose/envs/assets_compiler-jenkins.yml run --rm assets_compiler mimify:stylus')
    container(ctx, service=service, force=force)


@task(help={
    'service': 'service name [djcms]',
    'force': 'force [False]'
})
def prod_and_restart(ctx, service, force=False):
    """
    Build prod images and restart application with collecting statics
    """
    ctx.run('docker-compose -p acproddjcms -f compose/envs/assets_compiler-jenkins.yml run --rm assets_compiler mimify:stylus')
    container(ctx, service=service, force=force)

    dc(ctx, service, 'down')
    dc(ctx, service, 'up -d')
    django_run(ctx, service=service, command="collectstatic")

@task(help={
    'service': 'service name [djcms-prod]',
    'force': 'force [False]'
})
def prod_to_registry(ctx, service='djcms-prod', force=False):
    """
    Build prod locally and send to registry
    """
    ctx.run('docker-compose -p acproddjcms -f compose/envs/assets_compiler-jenkins.yml run --rm assets_compiler mimify:stylus')
    for container_name in ['django', 'nginx', 'postgres']:
        container(ctx, service=service, container=container_name, force=force)
        ctx.run('docker push {0}/djcms_{1}:latest'.format(ctx.registry, container_name))

