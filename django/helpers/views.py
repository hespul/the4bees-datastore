# -*- coding: utf-8 -*-
from django.views.generic import View
from django.views.decorators.csrf import csrf_exempt
from django.core.exceptions import PermissionDenied

from rest_framework_jwt.authentication import JSONWebTokenAuthentication
from rest_framework.exceptions import AuthenticationFailed

# TODO get / post / patch objet simple avec méthodes toutes faites + form + serializer + champs


class ApiView(View):
    @csrf_exempt
    def dispatch(self, request, *args, **kwargs):
        try:
            self.__set_request_user_from_jwt()
        except AuthenticationFailed:
            pass
        return super().dispatch(request, *args, **kwargs)

    def __set_request_user_from_jwt(self):
        jwt = JSONWebTokenAuthentication()
        auth = jwt.authenticate(self.request)
        if auth is None:
            return

        self.request.user = auth[0]
        return


class LoginRequiredApiView(ApiView):
    def __set_request_user_from_jwt(self):
        jwt = JSONWebTokenAuthentication()
        auth = jwt.authenticate(self.request)
        if auth is None:
            raise PermissionDenied

        self.request.user = auth[0]
        return


class AdministratorRequiredApiView(ApiView):
    def __set_request_user_from_jwt(self):
        jwt = JSONWebTokenAuthentication()
        auth = jwt.authenticate(self.request)
        if auth is None:
            raise PermissionDenied

        self.request.user = auth[0]
        if not self.request.user.is_administrator:
            raise PermissionDenied

        return
