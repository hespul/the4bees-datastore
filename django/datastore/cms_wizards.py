from cms.wizards.wizard_base import Wizard
from cms.wizards.wizard_pool import wizard_pool

from django.utils.translation import ugettext_lazy as _

from .forms import SensorBoxWizardForm


class SensorBoxWizard(Wizard):
    pass


sensorbox_wizard = SensorBoxWizard(
    title="SensorBox",
    weight=200,
    form=SensorBoxWizardForm,
    description=_("Créer une nouvelle SensorBox"),
)

wizard_pool.register(sensorbox_wizard)
