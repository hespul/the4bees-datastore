# Django-CMS-Base

Django-CMS prêt à servir pour déployer un site web rapidement.

Il est intégré avec le framework Foundation (v6). Ce qui permet la gestion :

* de colonnes sur la base d'une grille de 12
* la création d'onglets
* la création de caroussels d'images
* la gestion d'un formulaire contact basique
* la gestion d'une FAQ
* la gestion de newsletters
* un moteur de recherche sur le site


# Installation

Clonage du dépôt :

    git clone https://bitbucket.org/hespul/django-cms-base.git
    cd django-cms-base
    git submodule init
    git submodule update
    chmod -R 777 djcms-media sql

# Docker

Application utilisable en tant que conteneur docker. Lancement simplifié via invoke

    inv -l
    
    Available tasks:
    c                        Django shell_plus
    down                     Shutdown service container
    migrate                  Do django migrate
    migrations               Make django migrations
    restart                  Restart service container
    run                      Django manage.py
    up                       Launch service container
    upd                      Launch service container and detach
    build.base               Build base image service for django container and push it to registry
    build.container          Build service container
    build.prod               Build prod images
    build.prod-and-restart   Build prod images and restart application with collecting statics
    build.prod-to-registry   Build prod locally and send to registry
    database.backup          Save database for [service] in file [name]
    database.list            List available backups for service
    database.restore         Restore database for [service] from file [name]
    database.shell           Get a psql shell for service
    django.c                 Django shell_plus
    django.migrate           Do django migrate
    django.migrations        Make django migrations
    django.run               Django manage.py
    docker.bash              Run bash in service container
    docker.down              Shutdown service container
    docker.logs              Show logs for container in service
    docker.ps                ps service container
    docker.restart           Restart service container
    docker.run               Run command in service container
    docker.set-networks      Create needed custom networks in docker
    docker.up                Launch service container
    docker.upd               Launch service container and detach

## Construire l'image

    # compilation des CSS
    docker-compose -p acproddjcms -f compose/envs/assets_compiler-jenkins.yml run --rm assets_compiler_stylus build:css

    # Création des images docker
    docker-compose -p djcms -f compose/envs/djcms-prod.yml build django
    docker-compose -p djcms -f compose/envs/djcms-prod.yml build nginx
    docker-compose -p djcms -f compose/envs/djcms-prod.yml build postgres
    
    # Téléchargement sur le registry
    docker push registry.hespul.org:5000/djcms_django:latest
    docker push registry.hespul.org:5000/djcms_postgres:latest
    docker push registry.hespul.org:5000/djcms_nginx:latest

## Utilisation

Au travers d'un ficher *.yml*, ici en version minimaliste

    version: '3'
    
    volumes:
      postgres_data: {}
      django_nginx_shared_data: {}
    
    services:
      django:
        image: registry.hespul.org:5000/djcms_django:master
        user: django
        command: /gunicorn.sh
        volumes:
          - django_nginx_shared_data:/app/django/static
          - ../../djcms-media:/app/django/media
        links:
          - postgres
        env_file: ./djcms-prod.env
        networks:
          - default
        restart: unless-stopped
      
      postgres:
        image: registry.hespul.org:5000/djcms_postgres:master
        volumes:
          - postgres_data:/var/lib/postgresql/data
          - ../../sql:/backups
        env_file: ./djcms-prod.env
        restart: unless-stopped
    
      nginx:
        image: registry.hespul.org:5000/djcms_nginx:master
        volumes:
          - django_nginx_shared_data:/app/django/static
          - ../../djcms-media:/app/django/media
        ports:
          - "0.0.0.0:20000:80"
        links:
          - django
        env_file: ./djcms-prod.env
        restart: unless-stopped
    
      elastic:
        image: elasticsearch:2.4
        restart: unless-stopped
        logging:
          driver: "none"
        restart: unless-stopped


Le fichier *.env* associé doit définir les variables d'environnement suivantes :

    # Postgres
    POSTGRES_USER=djcms
    POSTGRES_PASSWORD=PASSWD
    DATABASE_URL=postgres://djcms:PASSWD@postgres/djcms
    
    # General settings
    DJANGO_SETTINGS_MODULE=config.settings
    DJANGO_SECRET_KEY=XXXXXXXXXXXXXXXXXXXXXXX
    DJANGO_ALLOWED_HOSTS=127.0.0.1
    DJANGO_DEFAULT_FROM_EMAIL=webmestre@hespul.org
    DJANGO_EMAIL_SUBJECT_PREFIX=[DCB]
    DJANGO_CSRF_COOKIE_SECURE=False
    DJANGO_ADMIN_URL=^admin/
    
    # Email server configuration
    EMAIL_PORT=25
    EMAIL_HOST=postfix
    DJANGO_SERVER_EMAIL=postfix
    DJANGO_SERVER_EMAIL=errors-djcms@hespul.org
    DEFAULT_FROM_EMAIL=errors-djcms@hespul.org
    
    # Security! Better to use DNS for this task, but you can use redirect
    DJANGO_SECURE_SSL_REDIRECT=False
    COMPRESS_ENABLED=True
    DJANGO_DEBUG=False
    LANGUAGE_CODE=fr
    
    DJANGO_SITE_URL=SITE.DOMAINE.TLD
    
    # Migration on startup
    DJANGO_MIGRATE=True
    
    # Collect static on startup
    DJANGO_COLLECTSTATIC=True

    # PIWIK
    PIWIK_SITE_ID=1
    PIWIK_URL=piwik.example.com
	# Set to True to prepend the site domain to the page title when tracking
	PIWIK_SET_DOCUMENT_TITLE=False
	# Set to True to enable client side DoNotTrack detection (default: False)
	PIWIK_SET_DO_NOT_TRACK=False
	# Set to True to disable all tracking cookies (default: False)
	PIWIK_DISABLE_COOKIES=False
	# Set to True to not use JavaScript to track visitors (default: False)
	PIWIK_WITHOUT_JS=False


## Authentification

Ajouter l'application **compte** avec l'url definie par la variable LOGIN_URL (default='/user/login/').


## Utilisation du moteur de recherche Elastic Search

Pour déclencher l'utilisation Elastic Search, il faut définir la variable d'environnement :

    USE_HAYSTACK=True


## Utilisation de la Fabrique à Contacts

Pour déclencher l'utilisation de la Fabrique à Contacts, il faut définir deux variables d'environnement et utiliser Redis

    USE_FAC=True
    USE_REDIS=True

Ajout à docker-compose

    redis:
      image: redis:latest
      restart: unless-stopped
  
    redis_worker:
      image: registry.hespul.org:5000/djcms_django:master
      user: django
      command: /start-redis.sh
      volumes:
        - django_nginx_shared_data:/app/django/static
        - ../../www.velo-et-voie-verte-en-beaujolais.fr/djcms-media:/app/django/media
        - ../../www.velo-et-voie-verte-en-beaujolais.fr/css:/app/django/djcms/static/css
        - ../../www.velo-et-voie-verte-en-beaujolais.fr/foundation_css:/app/django/foundation/static/foundation/css
        - ../../www.velo-et-voie-verte-en-beaujolais.fr/favicon:/app/django/core/static/favicon/
        - ../../www.velo-et-voie-verte-en-beaujolais.fr/templates:/app/django/core/templates
      links:
        - postgres
        - redis
      env_file: ../../www.velo-et-voie-verte-en-beaujolais.fr/www.velo-et-voie-verte-en-beaujolais.fr.env
      user: "1000:1000"
      networks:
        - default
      restart: unless-stopped

Ajouter également un lien networks de django vers redis

# Création d'un compte admin

    docker-compose -f SITE.yml -p SITE exec  django python /app/django/manage.py createsuperuser

# Personnalisation

## CSS

la feuille feuille de style est préprocessée avec
[Stylus](http://stylus-lang.com/). Les fichiers sont dans le répertoire
*./stylesheet/*.

La génération du css peut se faire via le lancement d'un conteneur docker : 

    docker-compose -p djcms-assets -f compose/envs/assets_compiler-jenkins.yml run --rm assets_compiler build:stylus

La feuille de style générée est copié dans *./django/static/css/project.css*.


### Foundation

Le template de base utilise Foundation v6. Vous pouvez le personnaliser depuis la page https://foundation.zurb.com/sites/download.html/
La grille utilisée est « XY grid ». Définissez les couleurs primaires et secondaires.

Le répertoire *django/foundation/static/foundation/* peut être surchargé avec le contenu de l'archive générée.


## Assets compiler

    docker-compose -p djcms -f compose/envs/assets_compiler-jenkins.yml build
    docker tag djcms_assets_compiler:latest registry.hespul.org:5000/djcms_assets_compiler:latest
    docker push registry.hespul.org:5000/djcms_assets_compiler:latest

## Template

Le template de base est décrit dans le répertoire *./django/core/templates/*.
Il est possible de personnaliser les templates en montant un autre répertoire à
la place de celui ci via Docker.


## Favicon

Il est possible de personnaliser l'icône du site en montant un autre répertoire à
la place de celui ci (*/app/django/core/static/favicon/*) via Docker.


## Utilisation pour construire un site

Pour démarrer un site basé sur django-cms-base, il suffit d'un fichier docker-compose.yml
qui reprenne les éléments suivants avec éventuellement une injection dans l'image docker
des css (site et foundation), du favicon.png et des templates voulus.

    version: '3'
    
    volumes:
      postgres_data: {}
      django_nginx_shared_data: {}
    
    services:
      django:
        image: registry.hespul.org:5000/djcms_django:master
        user: django
        command: /gunicorn.sh
        volumes:
          - django_nginx_shared_data:/app/django/static
          - ../../www.velo-et-voie-verte-en-beaujolais.fr/djcms-media:/app/django/media
          - ../../www.velo-et-voie-verte-en-beaujolais.fr/css:/app/django/core/static/css
          - ../../www.velo-et-voie-verte-en-beaujolais.fr/foundation_css:/app/django/foundation/static/foundation/css
          - ../../www.velo-et-voie-verte-en-beaujolais.fr/favicon:/app/django/core/static/favicon/
          - ../../www.velo-et-voie-verte-en-beaujolais.fr/templates:/app/django/core/templates
        links:
          - postgres
        env_file: ../../www.velo-et-voie-verte-en-beaujolais.fr/www.velo-et-voie-verte-en-beaujolais.fr.env
        networks:
          - default
        restart: unless-stopped
    
      postgres:
        image: registry.hespul.org:5000/djcms_postgres:master
        volumes:
          - postgres_data:/var/lib/postgresql/data
          - ../../www.velo-et-voie-verte-en-beaujolais.fr/sql:/backups
        env_file: ../../www.velo-et-voie-verte-en-beaujolais.fr/www.velo-et-voie-verte-en-beaujolais.fr.env
        restart: unless-stopped
    
      nginx:
        image: registry.hespul.org:5000/djcms_nginx:master
        volumes:
          - django_nginx_shared_data:/app/django/static
          - ../../www.velo-et-voie-verte-en-beaujolais.fr/djcms-media:/app/django/media
        ports:
          - "0.0.0.0:20002:80"
        links:
          - django
        env_file: ../../www.velo-et-voie-verte-en-beaujolais.fr/www.velo-et-voie-verte-en-beaujolais.fr.env
        restart: unless-stopped

Les CSS site et foundations doivent s'appeler project.css pour être appelés dans le template de base.


### Compilation des assets

La compilation des assets peut se faire au moyen du fichier docker-compose suivant :

    version: '3'
    
    services:
      assets_compiler_stylus:
        image: registry.hespul.org:5000/docker-assetscompiler:latest
        volumes:
          - ./css:/stylus_css
          - ./stylesheet:/stylus
          - ./foundation:/sass
          - ./foundation_css/:/sass_css
          - ./config:/config

Le container est appellé de la façon suivante :

    docker-compose -f assets.yml run --rm assets_compiler_stylus config build:sass mimify:sass build:stylus mimify:stylus


### Utilisation de postfix

[doc du conteneur](https://github.com/MarvAmBass/docker-versatile-postfix)

Pour utiliser postfix avec django-cms, ajouter cette section dans le fichier docker-compose 

    postfix:
      image: marvambass/versatile-postfix
      volumes:
        - ../../www.velo-et-voie-verte-en-beaujolais.fr/postfix_dkim:/etc/postfix/dkim/
      command: velo-et-voie-verte-en-beaujolais.fr docker:MY_PASSWORD
      restart: unless-stopped

Et ajouter le lien réseau entre le conteneur postfix et django :

    django:
      links:
        - postfix

Configurer les variables d'environnement :

    # Used with email
    EMAIL_PORT=25
    EMAIL_HOST=postfix
    EMAIL_HOST_USER=docker
    EMAIL_HOST_PASSWORD=MY_PASSWORD

Générer les clefs dkim :

    apt-get install opendkim-tools
    mkdir postfix_dkim ; cd postfix_dkim
    opendkim-genkey -s mail -d example.com
    mv mail.private dkim.key

Ajouter l'enregistrement DNS contenu dans mail.txt en tant que TXT-Record
pour mail._domainkey [selector._domainkey]

Vérifier le dns :

    host -t TXT mail._domainkey.example.com
    
    mail._domainkey.example.com descriptive text "v=DKIM1\; k=rsa\; p=MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQDcUp8Q1sbxgnR2iL7w+TOHN1IR6PzAP3vmUoPfeN07NGfWo8Wzxyn+hqqnC+mbPOW4ZDoAiu5dvpPsCt1RQalwBw/iPlB/8ScTlPGRpsTLo4ruCDL+yVkw32/UhvCL8vbZxM/Q7ELjO6AqRRW/KuCvbd5gNRYGeyjWd+UQAfmBJQIDAQAB"


# Leaflet Map usage

See https://github.com/makinacorpus/django-leaflet/tree/master/example


# ALLOWED_HOSTS configuration

Allowed hosts are configured using the env variable DJANGO_ALLOWED_HOSTS

    DJANGO_ALLOWED_HOSTS=127.0.0.1,192.168.1.4,172.16.1.7
    
If DJANGO_ALLOWED_HOSTS == * then allowed hosts are controlled by AllowedHost objects
which can be configured in the admin interface.

Be carefull, in this case if no AllowedHost exists, then access is granted for
everyone by default.
