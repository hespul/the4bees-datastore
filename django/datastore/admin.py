# -*- coding: utf-8 -*-
from django.contrib import admin
from django.utils.translation import ugettext_lazy as _

from .models import (
    DataPoint,
    Message,
    SensorBox,
)


class DataPointInline(admin.TabularInline):
    model = DataPoint


class DataPointAdmin(admin.ModelAdmin):
    base_model = DataPoint
    list_display = [
        'pk',
        'date',
        'temperature',
        'voc',
        'co2',
        'humidity',
        'lux',
        'sensor_box',
    ]
    list_filter = ('sensor_box',)
    exclude = []

    
class MessageAdmin(admin.ModelAdmin):
    base_model = Message
    list_display = [
        'pk',
        'mesure',
        'title',
        'min_value',
        'max_value',
        'start_date',
        'end_date',
        'icon_fa',
        'list_sensor_box',
    ]
    list_filter = ('sensor_box', 'mesure')
    exclude = []

    def list_sensor_box(self, obj):
        return ', '.join([str(x) for x in obj.sensor_box.all()])
    list_sensor_box.short_description = _('Sensor Box')

    def icon_fa(self, obj):
        fa = obj.fa_icon
        return f'<center style="color:{obj.icon_color};"><i class="far fa-{fa}"></i></center>'

    icon_fa.short_description = _('Icône')
    icon_fa.allow_tags = True

    class Media:
        js = ("font_awesome_5/js/fontawesome-all.min.js",)


class SensorBoxAdmin(admin.ModelAdmin):
    base_model = SensorBox
    list_display = [
        'pk',
        'title',
        'slug',
        'count_values',
        'last_point_date',
        'is_public',
        'list_owners',
    ]
    list_filter = ('is_public',)
    search_fields = ('title', 'list_owners', )
    readonly_fields = ['creation_date', 'last_modification', 'uuid']
    exclude = []
    # inlines = [DataPointInline, ]

    def list_owners(self, obj):
        return ', '.join([str(x) for x in obj.owners.all()])
    list_owners.short_description = _('Propriétaires')

    def count_values(self, obj):
        return obj.datapoint_set.count()
    count_values.short_description = _('Nombre de mesures')

    def last_point_date(self, obj):
        try:
            return obj.datapoint_set.last().date
        except AttributeError:
            return '-'
    last_point_date.short_description = _('Date de la dernière mesure')

    def list_is_public(self, obj):
        return obj.is_public
    list_is_public.short_description = _('Données publiques')
    list_is_public.boolean = True


admin.site.register(DataPoint, DataPointAdmin)
admin.site.register(Message, MessageAdmin)
admin.site.register(SensorBox, SensorBoxAdmin)
