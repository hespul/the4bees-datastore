# -*- coding: utf-8 -*-

from django.conf import settings
from django.conf.urls import include, url
from django.conf.urls.static import static
from django.contrib import admin

# from rest_framework_jwt.views import refresh_jwt_token
# from accounts.views import CustomObtainJSONWebToken

from django.views.generic.base import RedirectView

admin.autodiscover()


urlpatterns = [
    # JWT auth
    # url(r"^api/v1/auth/login", include("rest_framework.urls")),
    # url(r"^api/v1/auth/obtain_token/", CustomObtainJSONWebToken.as_view()),
    # url(r"^api/v1/auth/refresh_token/", refresh_jwt_token),
    url(r"^admin/", admin.site.urls),
    url(r'^datastore/', include('datastore.urls', namespace="datastore-api")),
    url(r"^$", RedirectView.as_view(url="/admin/", permanent=False), name="index"),
]

try:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
except:  # NOQA
    pass


if settings.DEBUG:
    urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)

    if "debug_toolbar" in settings.INSTALLED_APPS:
        import debug_toolbar

        urlpatterns = [
            url(r"^__debug__/", include(debug_toolbar.urls, namespace="debug_toolbar"))
        ] + urlpatterns
