class AsWebixOptionableMixin():
    """
    If as equal "webix_options" in serializer context, return simplified representation compatible with webix options :
    including only id and value => [{id:1, value:'blop'}, {id:2, value:'truc'}, ...]
    """
    def to_representation(self, obj, *args, **kwargs):
        if (
                self.context.get('as') == 'webix_options'
                or (
                    'request' in self.context
                    and 'as' in self.context['request'].query_params
                    and self.context['request'].query_params['as'] == 'webix_options'
                )
        ):
            return {
                'id': obj.id,
                'value': self.webix_option_value(obj)
            }
        return super(AsWebixOptionableMixin, self).to_representation(obj, *args, **kwargs)

    def webix_option_value(self, obj):
        """
        Should return a string for the webix option value
        """
        raise NotImplementedError('webix_option_value neeed by AsWebixOptionableMixin mixin')
